use strict;
use warnings;
package RT::UW::Theme;

our $VERSION = '0.01';

require RT::Config;
push @{$RT::Config::META{WebDefaultStylesheet}->{WidgetArguments}{Values}}, "UWinhouse";

1;


=head1 NAME

RT-UW-Theme - Customizations to a theme for UW (based on Rudder)

=head1 INSTALLATION 

=over

=item perl Makefile.PL

=item make

=item make install

May need root permissions

=item Edit your /opt/rt4/etc/RT_SiteConfig.pm

Add this line:

    Set(@Plugins, qw(RT::UW::Theme));

or add C<RT::UW::Theme> to your existing C<@Plugins> line.

=item Clear your mason cache

    rm -rf /opt/rt4/var/mason_data/obj

=item Restart your webserver

=back

=head1 AUTHOR

Jeff Voskamp <javoskam@uwaterloo.ca>

=head1 BUGS

All bugs should be reported via
L<rt-upgrade@rt.uwaterloo.ca>.


=head1 LICENSE AND COPYRIGHT

This software is Copyright (c) 2013 by Jeff Voskamp

This is free software, licensed under:

  The GNU General Public License, Version 2, June 1991

=cut

1;
